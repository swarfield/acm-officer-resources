Secretary
#####

:Author: Jack Garner

In this document I describe how things went as Secretary of ACM for the 2018-19
school year.

Pre-Semester Planning
=====================

Before the school started, most of the planning I had to do involved learning
how the sign in form worked and how to register rooms. Those were my main tasks
beyond just helping out whenever needed.

Advertising
===========

Our advertising efforts were extremely effective and attracted some of our
biggest events and meetings. I went to a few classes and gave pitches to them.
The pitches were super easy although make sure to have your laptop to display
the fliers since not all classes (Intro to Linux comes to mind) use formal
slides.

Celebration of Mines
--------------------

At the next Celebration of Mines, it would be great to have a real banner or
poster to use. We were also extremely busy but our limited space makes it a
little hard to have many volunteers. It's important to make sure the table
doesn't look more crowded than it already is.

At outreach events, parents tend to be big fans of old computers, but we
probably want to avoid being too reliant on Jack now that he's graduated. If we
can come up with some kind of club owned hardware, that would probably help.

Tech Talks
==========

For most of the first semester, we didn't have a working auto attendance link.
Instead we had to use the specific survey number links. It's useful to know
those exist.

Scheduling Rooms
================

The website for scheduling rooms can be a little painful to use. Despite this,
it's important to sign up for rooms early. During the first semester, we ended
up with plenty of suboptimal rooms and had to jump around a lot. Also, make sure
the room you get is a real classroom and not a computer lab. Lists of those are
online. After you sign up, you will receive confirmation emails from the
Registrar and some helpful instructions. One thing to remember is that you need
to contact someone listed in those instructions if food will be at the event.
Each semester, I just sent an email letting them know when/where we would be and
if there was anything special we needed to do. Their response was to just make
sure we clean up before leaving.

Forms
=====

Sending all confirmation emails to the rest of the team seems like a good idea.
I generally did this for any room reservations so everyone would have a copy in
case they needed it.
