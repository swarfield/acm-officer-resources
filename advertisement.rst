Advertisement
=============

:Owner: Sumner Evans

We need to advertise our club. There are three main sources of new members:
Celebration of Mines, CS classes, and referrals.

Celebration of Mines
--------------------

:Owner: Sumner Evans

Make sure that you sign up for a table with power at Celebration of Mines.
Things to have at the table include:

- Stickers for ACM, Firefox, Mozilla, OSSN
- Cool computers (in the past we have computers from Jack Rosenthal's
  collection, they are a great draw)
- Make sure that Tracy includes a question (or two) on the CSCI 101 scavenger
  hunt to encourage students to come to our booth at Celebration.

CS Classes
----------

:Owner: Sumner Evans

The CS Department as a whole is very supportive of ACM. We should attempt to get
one of the officers to present about ACM at every section of 101, 261, 262, 274,
303, and 306. Advertising at the upper level classes is not nearly as useful
since by then, most people know about ACM anyway.

At these presentations, explain what ACM is, what we do, how it's awesome, and
how to get involved. Ideally, we do this on week 1 so we can advertise our first
big tech talk and tell people to come to our booth at Celebration of Mines.

Referrals
---------

:Owner: Sumner Evans

This is one of our best sources of new members. People who are enthusiastic
about ACM will tell their friends to come.

We should encourage people to bring friends.
