Information for MHacks
======================

:Author: Sumner Evans
:Date: 2018-10-05

This document describes some of the essential information you will need for
MHacks. Please do not hesitate to contact me with questions.

**You are responsible for yourself. Please read and understand all of the
information in this document. Pay special attention to the Schedule, we will not
hesitate to leave you behind if you are not on time.**

Flight Details
--------------

+-----------------------------------------------------+---------------------------------------------------+
| Outbound (DEN to DTW)                               | Inbound (DTW to DEN)                              |
+=====================================================+===================================================+
| | **Airline:** Spirit                               | | **Airline:** Spirit                             |
| | **Flight No:** NK 976                             | | **Flight No:** NK 975                           |
| | **Departure:** Thurs., October 11 @ 19:10 (MDT)   | | **Departure:** Mon., October 15 @ 07:00 (CDT)   |
| | **Arrival:** Thurs., October 11 @ 23:59 (CDT)     | | **Arrival:** Mon., October 15 @ 08:11 (MDT)     |
| | **Flight Time:** 2:49                             | | **Flight Time:** 3:11                           |
+-----------------------------------------------------+---------------------------------------------------+

Apon arrival at DIA goto to Spirit's kiosks. Enter the confirmation number 
**J944FS** and your last name, Then book any baggage you want to take on.

Please see **Sam Warfield** at the gate before takeoff for final roll call.

For Drivers
^^^^^^^^^^^

Park in the Pikes Peak Lot, you will be reimbursed for parking. Any other lot 
will be at the driver's expense. 

Hotel Details
-------------

We will be booking 6 rooms in a hotel close to the airport for Thursday night
and Sunday night. I will send out the booking details as soon as I have them.

`Hotel Location <https://maps.google.com/?cid=11416746272223074353>`_

Baggage
-------

Due to the fact that prices on the flights were so high, we are not able to buy
checked/carry-on bags for anyone. Spirit allows everyone to have a personal item
like a backpack that you have to store under the seat in front of you.

**If you would like more than just the personal item**, Drivers will have one checked
bag for overflow. Make sure you coordinate with them.

Things to Pack
--------------

At a minimum, you should pack the following items:

- Laptop
- Phone
- Chargers for whatever devices you have
- Money
- Toiletries (make sure you pack **deodorant**!!!)
- Clothes for 4 days (Friday, Saturday, Sunday, Monday)

  **Note on weather**: looking at the forecast, the highs will be about 60
  degrees, and the lows will be in the mid 40s. Please pack accordingly.

- Anything else you need to survive for the trip (e.g. prescription medicines)

Some other things to consider bringing include:

- Random electronics (looking at you Robby)
- Ethernet cables (and ethernet adapters if you need them)
- Hammock (for sleeping)
- Pillow
- Power strip

Communication
-------------

Everyone should join our Matrix room ``#mines-acm-mhacks:matrix.org``. If you
do not know how to join Matrix, or are having trouble doing so, please reach out
to me or Robby.

This is also a great place to go if you don't have a team for the hackathon yet.

.. raw:: latex

    \newpage

Schedule
--------

Here's the schedule we have planned out. All of the important stuff is listed
here, but it's not meant to be a full play-by-play of what we do each day.

Thursday, October 11
^^^^^^^^^^^^^^^^^^^^

**Please note, if you are not on time, we will leave you behind and you will
have to figure out how to get to the airport yourself.**

| **16:00**: Meet in the IM parking lot to pack cars and leave for the airport
| **16:10**: Leave for the airport
| **19:10**: Outbound Flight departs from DEN to DTW
| **23:59**: Outbound Flight arrives in DTW

Friday, October 12
^^^^^^^^^^^^^^^^^^

| **Early in the morning**: Check into hotel near DTW
| **14:30**: Take Shuttle to MHacks
| **16:00**: Hackathon check-in starts (things actually start at about 18:00)

Saturday, October 13
^^^^^^^^^^^^^^^^^^^^

**All Day**: Hackathon!

Sunday, October 14
^^^^^^^^^^^^^^^^^^

| **17:00**: Hackathon closing ceremonies end
| **18:00**: Take shuttle to hotel near DTW

Monday, October 15
^^^^^^^^^^^^^^^^^^

| **05:00**: Check out of hotel and head to the airport
| **07:00**: Inbound Flight departs from DTW to DEN
| **08:11**: Inbound Flight arrives in DEN
| **~10:00**: Arrive back on campus
